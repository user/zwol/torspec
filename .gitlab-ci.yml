---
variables:
  SITE_URL: spec.torproject.org

build:
  image: rust:bookworm
  rules:
    # We install mermaid-cli only when we are building for the
    # actual website, since it is pretty expensive.  If we switch to
    # faster install tool, we should revisit that.
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "tpo" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      variables:
         MMDC: mmdc -p bin/ci-puppeteer-config.json
         # ^ See note
    # But we _do_ want to build unconditionally.
    - when: always
  script:
    - env
    - apt-get update && apt-get install -y git python3 python3-yaml
    - ./bin/via-cargo-install-in-ci mdbook mdbook-linkcheck
    - |
      if test "$MMDC" != "" ; then
         ./bin/mermaid-cli-in-ci
      else
         ./bin/via-cargo-install-in-ci mdbook-mermaid
      fi
    - ./bin/check_index
    - ./bin/build_html
    - mv html public
  artifacts:
    paths:
      - public
  cache:
    paths:
      - cache

# Note re MMDC above:
# https://github.com/mermaid-js/mermaid-cli/blob/HEAD/docs/linux-sandbox-issue.md
#
# bin/ is the wrong place for that file
# but I don't want to rename all of bin/ to maint/ right now.

link-check:
  image: rust:bookworm
  rules:
    - when: always
  script:
    - apt-get update && apt-get install -y git python3 python3-yaml linklint
    - ./bin/via-cargo-install-in-ci mdbook mdbook-linkcheck
    - ./bin/build_html
    - ./bin/check_links
  artifacts:
    paths:
      - html.link-check
  cache:
    paths:
      - cache

include:
  project: tpo/tpa/ci-templates
  file:
    - static-shim-deploy.yml
    - pages-deploy.yml

# use pages for merge requests, not main (which is the default in the
# above template). the logic here is that we want people that do not
# have acess to the repository to still have a preview of their merge
# request.
pages:
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE != "tpo" && $CI_PIPELINE_SOURCE == "merge_request_event"'
