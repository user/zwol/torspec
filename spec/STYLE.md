# Tor specifications: style and usage notes

## Audience

The primary intended audiences are:

 * Programmers:
   implementors of the Tor Protocols.
   This includes both maintainers of existing implementations,
   and people writing new implementations.

 * Researchers:
   people analysing the security of the Tor Protocols,
   including both academic research and
   practical security investigation.

 * Expert users who wish to fully understand
   the operation of their Tor software.
   This includes users of clients and relays.

 * Directory authority operators,
   and others with a critical technical role
   in the integrity of the Tor network.


## Scope and intentions

These notes apply to our specifications.  When possible, they should
also apply to proposals, to make proposals easier to merge into our
specifications when they are done.

As of 2023, our existing specifications have been written without any
real style guidelines, so you should not expect to find these
guidelines to apply well to all of the documents as you read them.
Instead, these guidelines are for documentation going forward.

These notes are not terribly well organized.  We should improve them
over time. They are meant to be a living document.



## Other sources

There are a number of other style guides used in Tor.  We should
follow these as well.  If they do not suit our needs, we should try to
get them changed.

* [Community team guidelines](https://gitlab.torproject.org/tpo/community/team/-/issues/5)
* [Tor project glossary](https://support.torproject.org/glossary/)
* [Specifications glossary](./glossary.md)

(Please add more if you know about them!)

As we refine the guidelines in this file, we should attempt to get
them upstreamed to more project-wide guides, if they are suitable.

## Starting notes

We are moving towards using
[semantic newlines](https://rhodesmill.org/brandon/2012/one-sentence-per-line/):
put each thought, or sentence, or subclause, on its own line,
in the Markdown source text.
(This is not yet applied consistently,
and we won't reject contributions that don't write that way.)

## Vocabulary

We use these terms freely:
  * Channel
  * Circuit
  * Stream

We try not to say "connection" without qualification: There are too
many things in Tor that can be called a "connection".

Similarly, don't say "session" without qualification except when it is
clear from context what we mean.

Prefer "relay" to "node" or "server".

Prefer "service" and "client" when talking about onion services and
their users.

Refer to arti as arti and the C tor implementation as "the C tor
implementation" on first mention.  Subsequently you can call it `tor`
or "C tor".

Avoid "AP" and "OP" and "OR"; those are not in current usage.

## Documenting keys

TODO: Explain our new key documentation conventions, as used
[here](./rend-spec/protocol-overview.html#imd-key-menagerie).

## Documentating data encodings

We have two competing legacy schemes for documenting data encodings.
One of them is an ad-hoc format the looks like this:
```
   * FIELD_1     [4 bytes]
   * FIELD_2     [1 byte]
```

The other is a somewhat C-like format based on the
[`trunnel` format](https://gitlab.torproject.org/tpo/core/trunnel/-/blob/main/doc/trunnel.md?ref_type=heads).
It looks like this:
```
struct message {
   u32 field_1;
   u8 field_2;
}
```

Neither of these is really great.  We should find something better.

## Writing explanations

> When you are writing an explanation in the middle of a bunch of
> normative text, it is a good idea to put it in quoted text, like
> this.


## Managing links

We're in the early stages of this spec organization, but we should
still be thinking about long term maintainability.

Please think about how to keep links working in the long term.
If you are going to add a link to a file, make sure that the file's
name is reasonable.  Before you rename a file, consider adding
a redirect from the file's old name.  (See the mdbook documentation
for more information about how.)

<div id="mdbook-heading-anchors">

If you want to link to a specific section within a file,
make sure that the section has a defined anchor that makes sense.
The syntax to define heading ids in mdbook looks like this:

`## Heading with a long title that you want shorter name for { #shortname }`

</div>

If you need to change a heading, make sure that you keep its
`id` the same as it was before, so that links will still work.

Finally, when you're looking for specific sections (e.g., to fix
references that say "See section 5.2.3") you can look for the HTML
anchors that our conversion process added.  For example,
if you want to find `dir-spec.txt` section 2.1.3, look for
the anchor that says `<a id="dir-spec.txt-2.1.3"></a>`.

## Specific Markdown syntax

### Define link fragment targets with `<span id="...">` (usually)

To manually make an target for a `#`-prefixed link fragment,
prefer `<span id="fragment">Text</span>`,
to `<a id=...>Text</a>`.
This works around mdbook mistakenly styling
`<a>` without `href`
as if it were a clickable link.

(Of course it is often better to
make the referenced text a section
and
[use the mdbook explicit anchor syntax](#mdbook-heading-anchors).)

You may need to make sure you have some other text
on the same line as the `<span>`
to avoid mdbook thinking you were writing
a whole paragraph of raw HTML.

Sometimes you may wish to use `<div id="...">` and `</div>`
(which must usually be placed as paragraphs of their own).

Example:
```
<span id="lemons-lemonade">LEMONS
are a sour fruit, sometimes used for making
lemonade</span>

(later)

Various fruit may be found, including [lemons](#lemons-lemonade).
```

It is OK to use an *empty* `a` element: `<a id=....></a>`.
Many existing section anchors are done this way.
