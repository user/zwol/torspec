<a id="pt-spec.txt-1"></a>

# Introduction

This specification describes a way to decouple protocol-level
obfuscation from an application's client/server code, in a manner
that promotes rapid development of obfuscation/circumvention
tools and promotes reuse beyond the scope of the Tor Project's
efforts in that area.

This is accomplished by utilizing helper sub-processes that
implement the necessary forward/reverse proxy servers that handle
the censorship circumvention, with a well defined and
standardized configuration and management interface.

Any application code that implements the interfaces as specified
in this document will be able to use all spec compliant Pluggable
Transports.

<a id="pt-spec.txt-1.1"></a>

## Requirements Notation

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
\[RFC2119\].
