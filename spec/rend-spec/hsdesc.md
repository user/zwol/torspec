<a id="rend-spec-v3.txt-2"></a>

# Generating and publishing hidden service descriptors \[HSDIR\]

Hidden service descriptors follow the same metaformat as other Tor
directory objects. They are published anonymously to Tor servers with the
HSDir flag, HSDir=2 protocol version and tor version >= 0.3.0.8 (because a
bug was fixed in this version).
