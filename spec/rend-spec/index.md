# Tor Rendezvous Specification - Version 3

This document specifies how the hidden service version 3 protocol works.  This
text used to be proposal 224-rend-spec-ng.txt.

This document describes a proposed design and specification for
hidden services in Tor version 0.2.5.x or later. It's a replacement
for the current rend-spec.txt, rewritten for clarity and for improved
design.
