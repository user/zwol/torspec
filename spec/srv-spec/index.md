# Tor Shared Random Subsystem Specification

This document specifies how the commit-and-reveal shared random subsystem of
Tor works. This text used to be proposal 250-commit-reveal-consensus.txt.
